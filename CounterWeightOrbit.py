# Nikko Rush

import matplotlib.pyplot as plt

from orbital import KeplerianElements, Position, Velocity, plot
from orbital.bodies import earth as Earth


def main():
    r = Position(0,100000*1000, 0)
    v = Velocity(1.157*1000,0,0)

    KeplerianElements.from_state_vector(r,v,Earth)
    elements = KeplerianElements.from_state_vector(r,v,Earth)

    plot(elements, animate=True)

    print(elements)
    plt.show()


if __name__ == "__main__":
    main()
